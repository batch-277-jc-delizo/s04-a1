-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 230.
SELECT * FROM songs where length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.artist_id = songs.album_id

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT artists.name, albums.album_title FROM artists 
    JOIN albums ON artist.id = albums.artist_id 
    WHERE album_title LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM songs ORDER BY album_name DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT albums.album_title, songs.song_name 
    FROM albums 
    INNER JOIN songs ON albums.id = songs.album_id 
    ORDER BY albums.album_title DESC;

