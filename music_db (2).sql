-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2023 at 11:13 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `date_released` date NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album_title`, `date_released`, `artist_id`) VALUES
(1, 'Trip', '1996-01-01', 999),
(2, 'Born Pink', '2022-09-16', 2),
(3, 'The album', '2020-10-02', 2),
(4, 'Midnights', '2022-10-21', 3),
(5, 'New Jeans', '2022-08-01', 4),
(6, 'Light Peace Love', '2005-01-01', 5),
(7, 'As the Music Plays', '2004-01-01', 5),
(8, 'Fearless', '2008-11-11', 3),
(9, 'Kill This Love', '2019-01-01', 2),
(12, 'A Star Is Born', '0000-00-00', 1004),
(13, 'Born This Way', '0000-00-00', 1004),
(14, 'Purpose', '0000-00-00', 1005),
(15, 'Believe', '0000-00-00', 1005),
(16, 'Dangerous Woman', '2016-01-01', 1006),
(17, 'Thank U, Next', '2019-01-01', 1006),
(18, '24K Magic', '0000-00-00', 1007),
(19, 'Earth to Mars', '0000-00-00', 1007);

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`) VALUES
(2, 'Blackpink'),
(3, 'Taylor Swift'),
(4, 'New Jeans'),
(5, 'Bamboo'),
(999, 'Rivermaya'),
(1002, 'Ariana Grande'),
(1003, 'Bruno Mars'),
(1004, 'Lady Gaga'),
(1005, 'Justin Bieber'),
(1006, 'Ariana Grande'),
(1007, 'Bruno Mars');

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE `playlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `playlist_name` varchar(30) NOT NULL,
  `datetime_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `playlist_songs`
--

CREATE TABLE `playlist_songs` (
  `id` mediumint(9) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `id` int(11) NOT NULL,
  `song_name` varchar(25) NOT NULL,
  `length` time NOT NULL,
  `genre` varchar(20) DEFAULT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `song_name`, `length`, `genre`, `album_id`) VALUES
(1, 'Snow on the Beach', '00:02:56', 'Pop', 4),
(2, 'Anti-Hero', '00:02:01', 'Pop', 4),
(3, 'Bejeweled', '00:03:14', 'Pop', 4),
(6, 'Masaya', '00:04:50', 'Jazz', 7),
(7, 'Mr. Clay', '00:03:57', 'Jazz', 7),
(11, 'Kill This Love', '00:03:18', 'KPOP', 2),
(12, 'Hope Not', '00:03:12', 'KPOP', 2),
(13, 'Bet You Wanna', '00:02:39', 'KPOP', 3),
(14, 'Lovesick Girls', '00:03:13', 'KPOP', 3),
(15, 'Ditto', '00:03:10', 'KPOP', 5),
(16, 'OMG', '00:03:22', 'KPOP', 5),
(17, 'Children of the sun', '00:03:33', 'Jazz', 6),
(18, 'Truth', '00:03:33', 'Jazz', 6),
(22, 'Black Eyes', '00:00:00', 'Rock and roll', 12),
(23, 'Shallow', '00:02:01', 'Country, rock, folk ', 12),
(24, 'Born This Way', '00:02:52', 'Electropop', 13),
(25, 'Sorry', '00:00:00', 'Dancehall-poptropica', 14),
(26, 'Boyfriend', '00:02:51', 'Pop', 15),
(27, 'Into You', '00:02:42', 'EDM house', 16),
(28, 'Thank U Next', '00:00:00', 'Pop, R&B', 17),
(29, '24K Magic', '00:02:07', 'Funk, disco, R&B', 18),
(30, 'Lost', '00:00:00', 'Pop', 19);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `contact_number` char(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_albums_artist_id` (`artist_id`);

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_playlists_user_id` (`user_id`);

--
-- Indexes for table `playlist_songs`
--
ALTER TABLE `playlist_songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_playlists_songs_playlist_id` (`playlist_id`),
  ADD KEY `fk_playlists_songs_song_id` (`song_id`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_songs_album_id` (`album_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1008;

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `playlist_songs`
--
ALTER TABLE `playlist_songs`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `fk_albums_artist_id` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `fk_playlists_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `playlist_songs`
--
ALTER TABLE `playlist_songs`
  ADD CONSTRAINT `fk_playlists_songs_playlist_id` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_playlists_songs_song_id` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `fk_songs_album_id` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
